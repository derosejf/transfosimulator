
export MODEL=gpt2-small
export MODEL_NAME=gpt2
export OUTPUT=$1

export TRAIN_FILE=./resources/gpt2/train.history_goal_action_usr_delex
export TEST_FILE=./resources/gpt2/val.history_goal_action_usr_delex

CUDA_VISIBLE_DEVICES=0 python trainer.py \
    --output_dir=/content/gdrive/MyDrive/DialogSystems_Project/$OUTPUT \
    --model_type=$MODEL \
    --model_name_or_path=$MODEL_NAME \
    --do_train \
    --train_data_file=$TRAIN_FILE \
    --do_eval \
    --eval_data_file=$TEST_FILE \
    --evaluate_during_training \
    --save_steps 7000 \
    --logging_steps -1 \
    --per_gpu_train_batch_size 4 \
    --per_gpu_eval_batch_size 16 \
    --gradient_accumulation_steps 4 \
    --learning_rate 5e-5 \
    --block_size 512 \
    --weight_decay 0.0 \
    --warmup_steps 0 \
    --max_grad_norm 1.0 \
    --eval_all_checkpoints \
    --seed 100 \
    --num_train_epochs 20