# -*- coding: utf-8 -*-
import re
import json
from argparse import ArgumentParser
import os
from transformers import GPT2Tokenizer
from MultiWozClass import MultiWozDataset
from utils.args_parser import ArgsParser
from tqdm import tqdm

gpt2_tokenizer = GPT2Tokenizer.from_pretrained('gpt2')
pattern = re.compile('<.*?>')

multiwoz_data = json.load(open('resources/multi-woz/delex.json', 'r'))
save_dir = './resources/gpt2'
os.makedirs(save_dir, exist_ok=True)

for split in ['train', 'val', 'test']:
    print('*** Preparing data for {}'.format(split))

    # load arguments
    args = ArgumentParser().parse_args()
    args.data_dir = './resources'
    args.use_action = True
    args.batch_size = 32
    args.seq_len = 512
    args.history_length = 5
    args.no_history = False
    args.overwrite_cache = True

    data = MultiWozDataset(args, split=split, shuffle=False)

    history_raw_new = []
    action_raw_new = []
    output_raw_new = []
    goal_raw_new = []

    if split == 'test':
        test_dict = {}

    # populate dictionary with data
    delex_dict = {}
    for d in data:
        delex_dict[d['name']] = d

    # iterate on conversations
    for ix, key in enumerate(delex_dict):
        d = delex_dict[key]
        inp = d['input_raw']
        out = d['target_raw']
        goal = multiwoz_data[key]['goal']

        for turn, (sys, usr) in enumerate(zip(inp, out)):
            # remember that usr comes before system
            # 0 - usr target
            # 1 - context usr, sys - usr is target
            if turn == 0:
                #idk what to do here yet
                history_new = '<|context|> <|endofcontext|>'
                pass
            else:
                tmp_new = ['<|context|>']
                for k in range(turn):

                    tmp_new.append('<|user|> ' + out[k].strip())
                    tmp_new.append('<|system|> ' + inp[k].strip())

                tmp_new.append('<|endofcontext|>')
                history_new = ' '.join(tmp_new)

            # update history and add response
            history_raw_new.append(history_new)
            output_raw_new.append('<|response|> ' + usr.strip() + ' <|endofresponse|>')

        # add action
        action = d['usr_act']
        for act in action:
            tmp_act_new = []
            for i, a in enumerate(act):
                if i == len(act) - 1:
                    tmp_act_new.append(' '.join(a))
                else:
                    tmp_act_new.append(' '.join(a))
            if len(tmp_act_new) == 0:
                tmp_act_new.append(' ')
            tmp_new = '<|action|> {} <|endofaction|>'.format(' , '.join(tmp_act_new))
            action_raw_new.append(tmp_new)

        # Parse goal into text
        def parse_goal(level, curr):
            outputs = []
            # Notes on parsing
            # skip message, topic
            # empty dict means skip
            # could have list at bottom level - need all of
            for key, value in level.items():
                if type(value) == bool:
                    value = str(value)
                if key in ['message', 'topic']:
                    continue
                if len(value) == 0:
                    continue

                # search the value
                if type(value) == dict:
                    tmp_outputs = parse_goal(level[key], curr+' '+key)
                    outputs.extend(tmp_outputs)
                elif type(value) == list:
                    for ans in value:
                        outputs.append(curr+' '+key+' '+ans)
                elif type(value) == str:
                    outputs.append(curr+' '+key+' '+value)
                else:
                    raise ValueError
            return outputs

        p_goal = parse_goal(goal, '')
        tmp_goal = ' , '.join(t.strip() for t in p_goal)
        format_goal = '<|belief|> {} <|endofbelief|>'.format(tmp_goal)

        for _ in range(len(action)):
            goal_raw_new.append(format_goal)

    # dataset name - history_belief_action_sys_delex
    # history, goal?, action, target
    out_file_name = '{}/{}.history_action_usr_delex'.format(save_dir, split)

    print('*** Writing to output file: {}'.format(out_file_name))
    tmp = []
    for hist, act, trg in zip(history_raw_new, action_raw_new, output_raw_new):
        tmp.append(' '.join([hist.lower(), act.lower(), trg.lower()]))
    with open(out_file_name, 'wt') as f:
        for ix, it in enumerate(tmp):
            if ix < 10:
                print('{} {} {}\n'.format(gpt2_tokenizer._bos_token, it.lower(), gpt2_tokenizer._eos_token))
            f.write('{} {} {}\n'.format(gpt2_tokenizer._bos_token, it.lower(), gpt2_tokenizer._eos_token))

    ## Writing second output file
    out_file_name = '{}/{}.history_goal_action_usr_delex'.format(save_dir, split)

    print('*** Writing to output file: {}'.format(out_file_name))
    tmp = []
    for hist, act, trg, goal in zip(history_raw_new, action_raw_new, output_raw_new, goal_raw_new):
        tmp.append(' '.join([hist.lower(), goal.lower(), act.lower(), trg.lower()]))
    with open(out_file_name, 'wt') as f:
        for ix, it in enumerate(tmp):
            if ix < 10:
                print('{} {} {}\n'.format(gpt2_tokenizer._bos_token, it.lower(), gpt2_tokenizer._eos_token))
            f.write('{} {} {}\n'.format(gpt2_tokenizer._bos_token, it.lower(), gpt2_tokenizer._eos_token))