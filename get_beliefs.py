
import os
os.chdir('C:\\Users\\rober\\Escritorio\\test')


from utils.multiwoz import dbPointer
from utils.multiwoz import delexicalize
from utils.multiwoz.nlp import normalize, normalize_lexical, normalize_beliefstate, normalize_mine
import json
from argparse import ArgumentParser

from MultiWozClass import MultiWozDataset


DATA_DIR = './resources'  #my resources, SIMPLETOD download
DATA_DIR2 = './resources2'  #resources on bitbucket


multiwoz_data = json.load(open(os.path.join(DATA_DIR, 'multi-woz/data.json')))  #? is this correct?

args = ArgumentParser().parse_args()
args.data_dir = './resources2'
args.use_action = True
args.batch_size = 32
args.seq_len = 512
args.history_length = 5
args.no_history = False
args.overwrite_cache = False

data = MultiWozDataset(args, split='train', shuffle=False)


def get_belief_state(bstate):
    domains = [u'taxi', u'restaurant', u'hospital', u'hotel', u'attraction', u'train', u'police']
    raw_bstate = []
    for domain in domains:
        for slot, value in bstate[domain]['semi'].items():
            if value:
                raw_bstate.append((domain, slot, normalize_beliefstate(value)))
        for slot, value in bstate[domain]['book'].items():
            if slot == 'booked':
                continue
            if value:
                new_slot = '{} {}'.format('book', slot)
                raw_bstate.append((domain, new_slot, normalize_beliefstate(value)))
    # ipdb.set_trace()
    return raw_bstate


history_raw_new = []
action_raw_new = []
output_raw_new = []
belief_raw_new = []

delex_dict = {}
for d in data:
    delex_dict[d['name']] = d



#SELECT KEY
#for key in delex_dict:
key = list(delex_dict.keys())[0]  

#OUR DATA    
d = delex_dict[key]
inp = d['input_raw']
out = d['target_raw']

#fill '<|user|> ' and '<|system|> ' for turn


#MULTI WOZ DATA
belief_raw_new = []
for turn in range(len(inp)):
    dialogue = multiwoz_data[key]
    goal = dialogue['goal']
    belief_state = get_belief_state(dialogue['log'][2*turn+1]['metadata'])  
    
    tmp_bs_new = []
    for i, b in enumerate(belief_state):
        if b[-1] in ['not mentioned']: # comment this for DST task
            continue
        if i == len(belief_state) - 1:
            tmp_bs_new.append(' '.join(b))
        else:
            tmp_bs_new.append(' '.join(b))
    
    if len(tmp_bs_new) == 0:
        tmp_bs_new.append(' ')
    
    tmp_new = '<|belief|> {} <|endofbelief|>'.format(' , '.join(tmp_bs_new))
    belief_raw_new.append(tmp_new)
    
belief_raw_new






