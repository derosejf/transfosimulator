# README #

### Quick Install ###

1) You must have conda installed on your local machine. This can be found online.

2) Run `conda env create -f environment.yml`. Activate the environment created by running
`conda activate ds_env`.

3) Run `conda install pytorch==1.5.1 torchvision==0.6.1 cudatoolkit=10.2 -c pytorch`.

4) Install Microsoft C++ Build Tools from https://visualstudio.microsoft.com/visual-cpp-build-tools/

5) Follow installation guide from ConvLab2 github to install ConvLab2 (should be a clone, cd
and pip command).