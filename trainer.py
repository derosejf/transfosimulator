# -*- coding: utf-8 -*-
"""
Script that trains an abstract model. Specific trainer class
is required for training this script.

Key things:
- If we want to implement and evaluate our simulator its best to
      build a end2end class and load it into an existing evaluation system
    - For raw training we should explicitly train it on the input data
    """

import os
import logging
import argparse
import json
import random
import time
import glob
import pickle
import re

import torch
import numpy as np
# import torch.nn as nn
# from torch.optim import Adam
from tqdm import tqdm, trange

from transformers import (WEIGHTS_NAME, GPT2Tokenizer, PreTrainedModel, PreTrainedTokenizer,
                          AdamW, get_linear_schedule_with_warmup)
from models import GPT2Config, GPT2LMHeadModel, GPT2SmallConfig

from data.dataset.language_model import (load_and_cache_examples, get_dataloader)
from utils.model import (_sorted_checkpoints, save_checkpoint)
from utils.gpt2_args_parser import ArgsParser

try:
    from torch.utils.tensorboard import SummaryWriter
except ImportError:
    from tensorboardX import SummaryWriter

# from convlab2.util.train_util import to_device

# DEVICE = 'cpu'
# DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")

#from convlab2.util.train_util import init_logging_handler
#from convlab2.task.multiwoz.goal_generator import GoalGenerator
#from convlab2.policy.vhus.multiwoz.usermanager import UserDataManager
#from convlab2.policy.vhus.train import VHUS_Trainer

# Model
# from convlab2.policy.vhus.multiwoz import UserPolicyVHUS
# from convlab2.util.dataloader.dataset_dataloader import MultiWOZDataloader
# from convlab2.util.dataloader.module_dataloader import ModuleDataloader

logging.basicConfig(
    format="%(asctime)s - %(levelname)s - %(name)s -   %(message)s",
    datefmt="%m/%d/%Y %H:%M:%S",
    level=logging.INFO
)
logger = logging.getLogger(__name__)


# class SimulatorE2EDataloader(ModuleDataloader):
#     '''
#     This is an inherited class that just specifies which attributes we would
#     like to pull from the MultiWoz dataset.
#
#     Options:
#         def load_data(self, data_dir=None, data_key='all', role='all',
#                           utterance=False, dialog_act=False, context=False,
#                           context_window_size=0, context_dialog_act=False,
#                           belief_state=False, last_opponent_utterance=False,
#                           last_self_utterance=False, goal=False)
#     '''
#     def load_data(self, *args, **kwargs):
#         kwargs.setdefault('goal', True)
#         kwargs.setdefault('dialog_act', True)
#         kwargs.setdefault('context', True)
#         kwargs.setdefault('context_window_size', 100)
#         kwargs.setdefault('belief_state', True)
#         kwargs.setdefault('terminated', True)
#         kwargs.setdefault('utterance', True)
#         return self.dataset_dataloader.load_data(*args, **kwargs)


# dataloader = SimulatorE2EDataloader(dataset_dataloader=MultiWOZDataloader())
#
# data = dataloader.load_data(data_key='val', role='usr')['val']
#
# for i in range(12):
#     print('Round:', i)
#     for k, v in data.items():
#         print(k, ":", v[i])
#         print("-" * 50)


MODEL_CLASSES = {
    "gpt2": (GPT2Config, GPT2LMHeadModel, GPT2Tokenizer),
    "gpt2-small": (GPT2SmallConfig, GPT2LMHeadModel, GPT2Tokenizer),
}


class Model():
    def __init__(self, args):
        '''
        This trains our model
        '''
        # self.dataloader = SimulatorE2EDataloader(dataset_dataloader=MultiWOZDataloader())
        self.model, self.tokenizer, self.model_class, self.args = self.get_model_tokenizer(args)
        self.evaluator = None # FIXME: look into MultiWozEvaluator
        self.optimizer = None
        self.scheduler = None

    def get_model_tokenizer(self, args):
        config_class, model_class, tokenizer_class = MODEL_CLASSES[args.model_type]

        if args.config_name:
            config = config_class.from_pretrained(args.config_name, cache_dir=args.cache_dir)
        elif args.model_name_or_path:
            config = config_class.from_pretrained(args.model_name_or_path, cache_dir=args.cache_dir)
        else:
            config = config_class()

        if args.tokenizer_name:
            tokenizer = tokenizer_class.from_pretrained(args.tokenizer_name, cache_dir=args.cache_dir)
        elif args.model_name_or_path:
            tokenizer = tokenizer_class.from_pretrained(args.model_name_or_path, cache_dir=args.cache_dir)
        else:
            raise ValueError(
                "You are instantiating a new {} tokenizer. This is not supported, but you can do it from another script, save it,"
                "and load it from here, using --tokenizer_name".format(tokenizer_class.__name__)
            )

        if args.block_size <= 0:
            args.block_size = tokenizer.model_max_length
            # Our input block size will be the max possible for the model
        else:
            args.block_size = min(args.block_size, tokenizer.model_max_length)

        if args.model_name_or_path:
            model = model_class.from_pretrained(
                args.model_name_or_path,
                from_tf=bool(".ckpt" in args.model_name_or_path),
                config=config,
                cache_dir=args.cache_dir,
            )
        else:
            logger.info("Training new model from scratch")
            model = model_class(config=config)

        model.to(args.device)

        if args.model_name_or_path == 'openai-gpt':
            tokenizer.add_special_tokens({'bos_token': '<|endoftext|>'})
            tokenizer.add_special_tokens({'eos_token': '<|endoftext|>'})
        elif args.model_name_or_path == 'gpt2':
            pass

        return model, tokenizer, model_class, args

    def train(self, args, train_dataset):
        print("Currently training the model")
        """ Train the model """
        tb_writer = SummaryWriter('./runs/{}'.format(args.output_dir.split('/')[-1]))

        # Prepare dataset
        train_dataset = load_and_cache_examples(args, self.tokenizer, evaluate=False)

        # Prepare dataloader
        train_dataloader, args = get_dataloader(train_dataset, self.tokenizer, args)

        # total iteration and batch size
        if args.max_steps > 0:
            t_total = args.max_steps
            args.num_train_epochs = args.max_steps // (len(train_dataloader) // args.gradient_accumulation_steps) + 1
        else:
            t_total = len(train_dataloader) // args.gradient_accumulation_steps * args.num_train_epochs

        total_batch_size = args.train_batch_size * args.gradient_accumulation_steps

        # Prepare optimizer and schedule (linear warmup and decay)
        self.optimizer, self.scheduler = self.get_optimizer_scheduler(args, t_total)

        # Train!
        print("Logger info is here")
        logger.info("***** Running training *****")
        logger.info("  Num examples = {}".format(len(train_dataset)))
        logger.info("  Num Epochs = {}".format(args.num_train_epochs))
        logger.info("  Instantaneous batch size per GPU = {}".format(args.per_gpu_train_batch_size))
        logger.info("  Total train batch size (w. parallel, distributed & accumulation) = {}".format(total_batch_size))
        logger.info("  Gradient Accumulation steps = {}".format(args.gradient_accumulation_steps))
        logger.info("  Total optimization steps = {}".format(t_total))

        global_step, epochs_trained, steps_trained_in_current_epoch = self.get_training_info(train_dataloader, args)

        tr_loss, logging_loss = 0.0, 0.0

        # FIXME: This might crash here
        self.model.resize_token_embeddings(len(self.tokenizer))
        self.model.zero_grad()

        train_iterator = trange(epochs_trained, int(args.num_train_epochs), desc="Epoch")

        for epoch in train_iterator:
            print("You are in the training iterator")
            global_step, tr_loss, logging_loss = self.train_epoch(train_dataloader, tr_loss, logging_loss, global_step,
                                                                  steps_trained_in_current_epoch, tb_writer, args, epoch)

            if args.max_steps > 0 and global_step > args.max_steps:
                train_iterator.close()
                break

        tb_writer.close()
        save_checkpoint(self.model, self.optimizer, self.scheduler, self.tokenizer, args, global_step)

        return global_step, tr_loss / global_step

    def train_epoch(self, train_dataloader, tr_loss, logging_loss, global_step, steps_trained_in_current_epoch, tb_writer, args, epoch):
        """train one epoch"""
        #epoch_iterator = tqdm(train_dataloader, desc="Iteration")
        ep_loss = 0.0
        ep_step = 0

        total_steps = len(train_dataloader) // args.gradient_accumulation_steps

        for step, batch in enumerate(train_dataloader):

            # Skip past any already trained steps if resuming training
            if steps_trained_in_current_epoch > 0:
                steps_trained_in_current_epoch -= 1
                continue

            inputs, labels = (batch, batch)
            inputs = inputs.to(args.device)
            labels = labels.to(args.device)
            self.model.train()
            outputs = self.model(inputs, labels=labels)
            loss = outputs[0]  # model outputs are always tuple in transformers (see doc)

            if args.gradient_accumulation_steps > 1:
                loss = loss / args.gradient_accumulation_steps

            loss.backward()

            tr_loss += loss.item()
            ep_loss += loss.item()

            if (step + 1) % args.gradient_accumulation_steps == 0:
                torch.nn.utils.clip_grad_norm_(self.model.parameters(), args.max_grad_norm)
                self.optimizer.step()
                self.scheduler.step()  # Update learning rate schedule
                self.model.zero_grad()
                global_step += 1
                ep_step += 1

                if global_step % 10 == 0:
                    step_loss = round(ep_loss / ep_step, 5)

                    logger.info("Epoch: {}, Global Step: {}, {} / {}, Loss: {}".format(
                        epoch+1, global_step, ep_step, total_steps, step_loss
                    ))

                # Log metrics
                if args.logging_steps > 0 and global_step % args.logging_steps == 0:
                    if (args.evaluate_during_training):
                        results = self.evaluate(args) # FIXME: not right params
                        for key, value in results.items():
                            tb_writer.add_scalar("eval_{}".format(key), value, global_step)
                    tb_writer.add_scalar("lr", self.scheduler.get_lr()[0], global_step)
                    tb_writer.add_scalar("loss", (tr_loss - logging_loss) / args.logging_steps, global_step)
                    logging_loss = tr_loss

                # save checkpoint
                if args.save_steps > 0 and global_step % args.save_steps == 0:
                    if args.evaluate_during_training:
                        save_checkpoint(self.model, self.optimizer, self.scheduler, self.tokenizer, args, global_step)

            if args.max_steps > 0 and global_step > args.max_steps:
                break

        return global_step, tr_loss, logging_loss

    def get_training_info(self, dataloader, args):
        global_step = 0
        epochs_trained = 0
        steps_trained_in_current_epoch = 0

        # Check if continuing training from a checkpoint
        if args.model_name_or_path and os.path.exists(args.model_name_or_path):
            try:
                # set global_step to gobal_step of last saved checkpoint from model path
                checkpoint_suffix = args.model_name_or_path.split("-")[-1].split("/")[0]
                global_step = int(checkpoint_suffix)
                epochs_trained = global_step // (len(dataloader) // args.gradient_accumulation_steps)
                steps_trained_in_current_epoch = global_step % (len(dataloader) // args.gradient_accumulation_steps)

                logger.info("  Continuing training from checkpoint, will skip to saved global_step")
                logger.info("  Continuing training from epoch %d", epochs_trained)
                logger.info("  Continuing training from global step %d", global_step)
                logger.info("  Will skip the first %d steps in the first epoch", steps_trained_in_current_epoch)
            except ValueError:
                logger.info("  Starting fine-tuning.")
        return global_step, epochs_trained, steps_trained_in_current_epoch

    def evaluate(self, args, prefix=""):
        eval_output_dir = args.output_dir

        eval_dataset = load_and_cache_examples(args, self.tokenizer, evaluate=True)

        os.makedirs(eval_output_dir, exist_ok=True)

        # Prepare dataloader
        eval_dataloader, args = get_dataloader(eval_dataset, self.tokenizer, args, split='eval')

        # Eval!
        logger.info("***** Running evaluation {} *****".format(prefix))
        logger.info("  Num examples = {}".format(len(eval_dataset)))
        logger.info("  Batch size = {}".format(args.eval_batch_size))
        eval_loss = 0.0
        nb_eval_steps = 0
        self.model.eval()

        for batch in tqdm(eval_dataloader, desc="Evaluating"):
            inputs, labels = (batch, batch)
            inputs = inputs.to(args.device)
            labels = labels.to(args.device)

            with torch.no_grad():
                outputs = self.model(inputs, labels=labels)
                lm_loss = outputs[0]
                eval_loss += lm_loss.mean().item()
            nb_eval_steps += 1

        eval_loss = eval_loss / nb_eval_steps
        perplexity = torch.exp(torch.tensor(eval_loss))

        result = {"perplexity": perplexity}

        output_eval_file = os.path.join(eval_output_dir, prefix, "eval_results.txt")
        with open(output_eval_file, "w") as writer:
            logger.info("***** Eval results {} *****".format(prefix))
            for key in sorted(result.keys()):
                logger.info("  %s = %s", key, str(result[key]))
                writer.write("%s = %s\n" % (key, str(result[key])))

        return result

    def get_optimizer_scheduler(self, args, t_total):
        '''
        Builds an optimizer
        '''
        no_decay = ["bias", "LayerNorm.weight"]
        optimizer_grouped_parameters = [
            {"params": [p for n, p in self.model.named_parameters() if not any(nd in n for nd in no_decay)],
                "weight_decay": args.weight_decay},
            {"params": [p for n, p in self.model.named_parameters() if any(nd in n for nd in no_decay)],
             "weight_decay": 0.0},
        ]
        optimizer = AdamW(optimizer_grouped_parameters, lr=args.learning_rate, eps=args.adam_epsilon)
        scheduler = get_linear_schedule_with_warmup(
            optimizer, num_warmup_steps=args.warmup_steps, num_training_steps=t_total
        )

        # Check if saved optimizer or scheduler states exist
        if (
                args.model_name_or_path
                and os.path.isfile(os.path.join(args.model_name_or_path, "optimizer.pt"))
                and os.path.isfile(os.path.join(args.model_name_or_path, "scheduler.pt"))
        ):
            # Load in optimizer and scheduler states
            optimizer.load_state_dict(torch.load(os.path.join(args.model_name_or_path, "optimizer.pt")))
            scheduler.load_state_dict(torch.load(os.path.join(args.model_name_or_path, "scheduler.pt")))

        return optimizer, scheduler

    def save_model(self, epoch=-1):
        all_state = {
            'states': self.model.state_dict(),
            'config': self.args.__dict__,
            'epoch': epoch
        }
        torch.save(all_state, self.args.model_output)


def set_seed(args):
    torch.manual_seed(args.seed)
    torch.cuda.manual_seed(args.seed)
    random.seed(args.seed)
    np.random.seed(args.seed)
    return


def main():
    args = ArgsParser().parse()

    if args.eval_data_file is None and args.do_eval:
        raise ValueError(
            "--eval_data_file should be specified when do_eval is true"
        )
    if args.should_continue:
        sorted_checkpoints = _sorted_checkpoints(args)
        if len(sorted_checkpoints) == 0:
            raise ValueError("--should_continue is true, but no checkpoint found in --output_dir")
        else:
            args.model_name_or_path = sorted_checkpoints[-1]

    # Setup CUDA, GPU & distributed training
    device = torch.device("cuda" if torch.cuda.is_available() and not args.no_cuda else "cpu")
    args.n_gpu = 0 if args.no_cuda else torch.cuda.device_count()
    args.device = device

    # Setup logging

    logger.warning(
        "Device: %s, n_gpu: %s",
        device,
        args.n_gpu
    )

    # Set seed
    set_seed(args)

    # Load pretrained model and tokenizer
    transfosimulator = Model(args)
    model_class = transfosimulator.model_class

    logger.info("Training/evaluation parameters {}".format(args))

    # Training
    if args.do_train:
        global_step, train_loss = transfosimulator.train(args, args.train_data_file)
        logger.info(" global_step = {}, average loss = {}".format(global_step, train_loss))

    # FIXME: Evaluation - hacky workaround, not sure if it will work though
    # Note - all checkpoints get evaluated together at the end of training
    results = {}
    if args.do_eval:
        checkpoints = [args.output_dir]

        if args.eval_all_checkpoints:
            checkpoints = list(
                os.path.dirname(c) for c in sorted(glob.glob(args.output_dir + "/**/" + WEIGHTS_NAME, recursive=True))
            )
            logging.getLogger("models.modeling_utils").setLevel(logging.WARN)  # Reduce logging
        logger.info("Evaluate the following checkpoints: {}".format(checkpoints))

        for checkpoint in checkpoints:
            global_step = checkpoint.split("-")[-1] if len(checkpoints) > 1 else ""
            prefix = checkpoint.split("/")[-1] if checkpoint.find("checkpoint") != -1 else ""

            model = model_class.from_pretrained(checkpoint)
            model.to(args.device)
            transfosimulator.model = model
            result = transfosimulator.evaluate(args, prefix=prefix)
            result = dict((k + "_{}".format(global_step), v) for k, v in result.items())
            results.update(result)

    return results

if __name__ == '__main__':
    main()
