import os
import sys
import random
import numpy as np
import pandas as pd
import torch
from tqdm import tqdm

from convlab2.nlu.jointBERT.multiwoz import BERTNLU
from convlab2.dst.rule.multiwoz import RuleDST
from convlab2.policy.rule.multiwoz import RulePolicy
from convlab2.policy.vhus.multiwoz import UserPolicyVHUS
from convlab2.nlg.template.multiwoz import TemplateNLG
from convlab2.dialog_agent import PipelineAgent, Session, Agent
from convlab2.evaluator.multiwoz_eval import MultiWozEvaluator
from convlab2.util.dataloader.dataset_dataloader import MultiWOZDataloader

from convlab2.util.dataloader.dataset_dataloader import MultiWOZDataloader
from convlab2.util.dataloader.module_dataloader import ModuleDataloader

from transfosimulator import Transfosimulator

SAMPLES = 100
out_file = 'results_gpt.csv'

class Sampler(ModuleDataloader):
    '''
    This is an inherited class that just specifies which attributes we would
    like to pull from the MultiWoz dataset.

    Options:
        def load_data(self, data_dir=None, data_key='all', role='all',
                          utterance=False, dialog_act=False, context=False,
                          context_window_size=0, context_dialog_act=False,
                          belief_state=False, last_opponent_utterance=False,
                          last_self_utterance=False, goal=False)
    '''
    def load_data(self, *args, **kwargs):
        kwargs.setdefault('goal', True)
        return self.dataset_dataloader.load_data(*args, **kwargs)


class BiSession(Session):
    def __init__(self, sys_agent: Agent, user_agent: Agent, kb_query=None, evaluator=None):
        self.sys_agent = sys_agent
        self.user_agent = user_agent
        self.kb_query = kb_query
        self.evaluator = evaluator

        self.dialog_history = []
        self.__turn_indicator = 0

        self.init_session()

    def next_agent(self):
        """The user and system agent response in turn."""
        if self.__turn_indicator % 2 == 0:
            next_agent = self.user_agent
        else:
            next_agent = self.sys_agent
        self.__turn_indicator += 1
        return next_agent

    def next_response(self, observation):
        next_agent = self.next_agent()
        response = next_agent.response(observation)
        return response

    def next_turn(self, last_observation):
        user_response = self.next_response(last_observation)
        if self.evaluator:
            self.evaluator.add_sys_da(self.user_agent.get_in_da())
            self.evaluator.add_usr_da(self.user_agent.get_out_da())
        session_over = self.user_agent.is_terminated()
        if hasattr(self.sys_agent, 'dst'):
            self.sys_agent.dst.state['terminated'] = session_over

        reward = self.user_agent.get_reward()
        sys_response = self.next_response(user_response)
        self.dialog_history.append([self.user_agent.name, user_response])
        self.dialog_history.append([self.sys_agent.name, sys_response])

        return sys_response, user_response, session_over, reward

    def train_policy(self):
        self.sys_agent.policy.train()

    def init_session(self, **kwargs):
        self.sys_agent.init_session()
        self.user_agent.init_session(**kwargs)
        if self.evaluator:
            self.evaluator.add_goal(self.user_agent.policy.get_goal())
        self.dialog_history = []
        self.__turn_indicator = 0


def set_seed(r_seed):
    random.seed(r_seed)
    np.random.seed(r_seed)
    torch.manual_seed(r_seed)


def main():
    custom = len(sys.argv) > 1

    # Build our system
    sys_nlu = BERTNLU()
    sys_dst = RuleDST()
    sys_policy = RulePolicy()
    sys_nlg = TemplateNLG(is_user=False)
    sys_agent = PipelineAgent(sys_nlu, sys_dst, sys_policy, sys_nlg, 'sys')

    # Build our user
    if not custom:
        print('******* Loading generic user simulator')
        user_nlu = BERTNLU()
        user_dst = None
        #user_policy = RulePolicy(character='usr')
        user_policy = UserPolicyVHUS(load_from_zip=True)
        user_nlg = TemplateNLG(is_user=True)
        user_agent = PipelineAgent(user_nlu, user_dst, user_policy, user_nlg, name='user')
    else:
        print('******* Loading GPTSimulator')
        user_agent = Transfosimulator('GPTSimulator')

    # Load Dataset
    dataloader = Sampler(dataset_dataloader=MultiWOZDataloader())
    data = dataloader.load_data(data_key='val', role='usr')['val']['goal']

    evaluator = MultiWozEvaluator()

    # Define our session
    sess = BiSession(sys_agent=sys_agent, user_agent=user_agent, kb_query=None, evaluator=None)

    # Sample
    set_seed(420)

    print('******* Starting Sampling')
    log = []
    samples = np.random.choice(np.arange(len(data)), SAMPLES, replace=False)
    for dix, ix in enumerate(tqdm(samples)):
        goal = data[ix]
        sys_response = ''

        if custom:
            sess.init_session(goal=goal)
            #print('Init Goal: {}'.format(sess.user_agent.parsed_goal))
        else:
            sess.init_session()

        # Do conversation sampling
        #print('-'*50)
        thanks_count = 0
        for i in range(20):
            sys_response, user_response, _, _ = sess.next_turn(sys_response)
            #print('user:', user_response)
            #print('sys:', sys_response)
            #print()

            if 'thank' in sys_response.lower():
                thanks_count += 1
            if thanks_count > 0:
                break

        # Write the dialog out
        label = 'dial_{}'.format(dix)
        log.append([label, 'goal', goal])
        for t, tup in enumerate(sess.dialog_history):
            name = 'usr'
            if t % 2 == 1:
                name = 'sys'
            log.append([label, name, tup[1]])

    # Done dialogs
    print('-'*50)
    print('********* Writing Dialogs to', out_file)
    pd.DataFrame(log).to_csv(out_file, index=False, header=False)
    return

if __name__ == '__main__':
    main()
