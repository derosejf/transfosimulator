
To Do

New Dataset Tasks
- implement goal or user belief into training data
- debug training script with new data
- (optional) check out shuffle context dataset if we chose to use it

VM Tasks
- test work on VM with simpletod data
- fix packages on VM
- update VM memory size -> 6+ MB
- update repo on VM
- Hook up cloud credits (Luis and Joe)
- Actually run simpletod data for 1-3 epochs