# -*- coding: utf-8 -*-
"""
Created on Fri Apr 23 22:05:57 2021

@author: rober
"""

import os
import csv    
os.chdir('C:\\Users\\rober\\Escritorio\\usim\\bitbuckProj\\transfosimulator')

u_path = 'resources\\gpt2\\train.history_goal_action_usr_delex'
s_path = 'resources\\gpt2\\train.history_belief_action_sys_delex'
with open(u_path, 'r') as dialog:
    u_dial_line = dialog.readlines()
    
with open(s_path, 'r') as dialog:
    s_dial_line = dialog.readlines()

beg_char = '<|response|>'
end_char = '<|endofresponse|>'
u_responses = []
s_responses = []
    
for line in u_dial_line:
    b = line.find(beg_char)
    e = line.find(end_char)
    u_responses.append(line[b+len(beg_char):e])
    
for line in s_dial_line:
    b = line.find(beg_char)
    e = line.find(end_char)
    s_responses.append(line[b+len(beg_char):e])
    
u_s_dialog = []
turn = []
pu = 0
ps = 0

for i in range(len(u_responses)+len(s_responses)):
    if i%2==0:
        u_s_dialog.append(u_responses[pu])
        turn.append(0)
        pu+=1
    else:
        u_s_dialog.append(s_responses[ps])
        turn.append(1)
        ps+=1
        
wrpath =  'resources\\generated_convs\\perplex_data.txt'      



with open(wrpath, 'w') as f:
    writer = csv.writer(f, delimiter='\t')
    writer.writerows(zip(turn,u_s_dialog))
        
    
    




