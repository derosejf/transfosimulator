# -*- coding: utf-8 -*-
"""
Created on Fri Apr 23 18:42:38 2021

@author: rober
"""

import sys, os
import numpy as np
import torch
from torch import multiprocessing as mp
from convlab2.dialog_agent.agent import PipelineAgent
from convlab2.dialog_agent.env import Environment
from convlab2.nlu.svm.multiwoz import SVMNLU
from convlab2.dst.rule.multiwoz import RuleDST
from convlab2.policy.rule.multiwoz import RulePolicy
from convlab2.policy.ppo import PPO
from convlab2.policy.rlmodule import Memory, Transition
from convlab2.nlg.template.multiwoz import TemplateNLG
from convlab2.evaluator.multiwoz_eval import MultiWozEvaluator
from argparse import ArgumentParser
import random
from convlab2.nlu.milu.multiwoz import MILU


DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
DEVICE


import argparse
parser = ArgumentParser()
parser.add_argument("--batchsz", type=int, default=1024, help="batch size of trajactory sampling")
parser.add_argument("--epoch", type=int, default=200, help="number of epochs to train")
parser.add_argument("--process_num", type=int, default=1, help="number of processes of trajactory sampling")
args = parser.parse_args(args=[])

# simple rule DST
dst_sys = RuleDST()

policy_sys = PPO(True)

# rule policy
policy_usr = RulePolicy(character='usr')
# assemble
simulator = PipelineAgent(None, None, policy_usr, None, 'user')

evaluator = MultiWozEvaluator()
env = Environment(None, simulator, None, dst_sys, evaluator)

sampled_num = 0
sampled_traj_num = 0
traj_len = 50
real_traj_len = 0

def set_seed(r_seed):
    random.seed(r_seed)
    np.random.seed(r_seed)
    torch.manual_seed(r_seed)

set_seed(30)


state = env.reset()

env.usr.policy.get_goal()

u_nlu = MILU()
u_dst = None
u_policy = RulePolicy(character='usr')
u_nlg = TemplateNLG(is_user=True)
u_sim = PipelineAgent(u_nlu, u_dst, u_policy, u_nlg, name='user')
set_seed(30)
u_sim.init_session()

u_sim.response([])

#this is the response