# -*- coding: utf-8 -*-
"""
Created on Fri Apr 23 21:01:14 2021

@author: rober
"""
import sys, os, pdb, re
import nltk
import os

from collections import defaultdict
from nltk import word_tokenize

from language_model.counter import build_vocabulary, count_ngrams
from language_model.ngram import LidstoneNgramModel

import numpy as np
from language_model.util import safe_div

class auto_eval(object):
    def __init__(self, path):

        with open(path, 'r') as dialog:
            self.dial_line = dialog.readlines()

        self.split = ','
        self.usr_utt = self._extract_utt()
        self.delex_usr_utt = self._delex_file()
        #self.test_file_path = '/data/qkun/simulator/data/multiwoz-master/data/multi-woz/restaurant.csv'
        #self.test_file = self._extract_test_file(self.test_file_path)

    def _extract_utt(self):
        # # utt : list -> list -> string
        utt = []
        for line in self.dial_line:
            if line.split(',')[1] == 'usr':
                utt.append(word_tokenize(','.join(line.split(',')[2:-1])))
        return utt

    def _delex_file(self):
        delex_usr_utt = []
        for utt in self.usr_utt:
            #delex_utt = delex_sent(' '.join(utt)).replace('[', '')
            delex_utt = ' '.join(utt).replace('[', '')
            delex_utt = re.sub(r'\|.*?\]', '', delex_utt)
            delex_usr_utt.append(word_tokenize(delex_utt))
        return delex_usr_utt


    def _extract_test_file(self, path=None):
        test_usr_utt = []
        with open(path, 'r') as test_file:
            for line in test_file.readlines()[1:10000]:
                # pdb.set_trace()
                if line.split(',')[2] == '0':
                    utt = ','.join(line.split(',')[3:]).replace('[','').replace('"','')
                    utt = re.sub(r'\|.*?\]','',utt)
                    test_usr_utt.append(word_tokenize(utt))
        return test_usr_utt


    def _perplexity(self):

        sequences = self.delex_usr_utt
        vocab = build_vocabulary(1, *sequences)
        counter = count_ngrams(3, vocab, sequences, pad_left=True, pad_right=False)
        model = LidstoneNgramModel(0.1, counter)

        ppl_per_word = 0
        avg_ppl = 0

        for utt in self.test_file:
            ppl = model.perplexity(' '.join(utt))
            avg_ppl += ppl
            ppl_per_word += ppl / len(utt)
        ppl_per_word /= len(self.test_file)
        avg_ppl /= len(self.test_file)

        return ppl_per_word, avg_ppl

    def mutlti_count(self):
        '''
        this func returns:
        average dialog length(# of turns)
        average utterance length(# of tokens), only for user
        vocab size, only for user
        '''
        dial_num = 0
        token_num = 0.
        turn_num = 0.
        dial_id = ''
        vocab = set()

        for line in self.dial_line:
            sep = line.split(self.split)

            if dial_id != sep[0]:
                dial_id = sep[0]
                dial_num += 1
            speaker = sep[1]
            sent = self.split.join(sep[2:-1])
            if speaker == 'usr':
                tokens = word_tokenize(sent.strip())
                if len(tokens) == 0:
                    continue
                turn_num += 1
                token_num += len(tokens)
                vocab.update(set(tokens))

        return turn_num/dial_num, token_num/turn_num, len(vocab)

    def usr_act_dist(self):
        # # this func returns a frequency distribution
        # # of 7 user dialog act
        act_distribution = {'INFORM_TYPE':0,
                            'INFORM_TYPE_CHANGE':0,
                            'ASK_INFO':0,
                            'MAKE_RESERVATION':0,
                            'MAKE_RESERVATION_CHANGE_TIME':0,
                            'ANYTHING_ELSE':0,
                            'GOODBYE':0}

        #predictor = usr_act_predictor()
        predictor = []
        
        for utt in self.usr_utt:
            # pdb.set_trace()
            act = predictor.predict(' '.join(utt)).upper()
            act_distribution[act] += 1

        # for line in self.dial_line:
        #     speaker = line.split(',')[1]
        #     if speaker == 'usr':
        #         sent = ','.join(line.split(self.split)[2:-1])
        #         act = line.split(self.split)[-1].replace('\n','').upper()

        #         act_distribution[act] += 1
        return act_distribution

path = 'results_convlab.csv'
Eval = auto_eval(path)
dial_len, sent_len, vocab_size = Eval.mutlti_count()
print('average dialog length: {} turns \naverage sentence length: {} words\nvocabulary size: {}\n'.format(
          dial_len, sent_len, vocab_size))

ppl_per_word, perplexity = Eval._perplexity()
print('average perplexity per word: %0.3f\naverage perplexity: %0.3f\n' %(ppl_per_word, perplexity))


'''

def main():

    #    corpus_path = '/home/wyshi/simulator/evaluation/dial_log/' + file_name + '.csv'
    corpus_path = 'resoures/generated_convs/eval_conv.csv'
    print('--'*20)
    print(corpus_path)
    # corpus_path = '/home/wyshi/simulator/evaluation/dial_log/seq_generation.csv'
    Eval = auto_eval(corpus_path)

    dial_len, sent_len, vocab_size = Eval.mutlti_count()
    print('average dialog length: %0.3f turns \naverage sentence length: %0.3f words\nvocabulary size: %d\n'.format(
          dial_len, sent_len, vocab_size))

    ppl_per_word, perplexity = Eval._perplexity()
    print('average perplexity per word: %0.3f\naverage perplexity: %0.3f\n' %(ppl_per_word, perplexity))


if __name__ == '__main__':
    main()
    
'''