
import os
#os.chdir('C:\\Users\\rober\\Escritorio\\test')


from utils.multiwoz import dbPointer
from utils.multiwoz import delexicalize
from utils.multiwoz.nlp import normalize, normalize_lexical, normalize_beliefstate, normalize_mine
import json
from argparse import ArgumentParser
from MultiWozClass import MultiWozDataset
from transformers import GPT2Tokenizer

gpt2_tokenizer = GPT2Tokenizer.from_pretrained('gpt2')

multiwoz_data = json.load(open('./resources/multi-woz/data.json', encoding="utf-8"))  #? is this correct?
save_dir = './resources/gpt2'
os.makedirs(save_dir, exist_ok=True)

def get_belief_state(bstate):
    domains = [u'taxi', u'restaurant', u'hospital', u'hotel', u'attraction', u'train', u'police']
    raw_bstate = []
    for domain in domains:
        for slot, value in bstate[domain]['semi'].items():
            if value:
                raw_bstate.append((domain, slot, normalize_beliefstate(value)))
        for slot, value in bstate[domain]['book'].items():
            if slot == 'booked':
                continue
            if value:
                new_slot = '{} {}'.format('book', slot)
                raw_bstate.append((domain, new_slot, normalize_beliefstate(value)))
    # ipdb.set_trace()
    return raw_bstate









for split in ['train', 'val', 'test']:
    print('*** Preparing data for {}'.format(split))

    # load arguments
    args = ArgumentParser().parse_args()
    args.data_dir = './resources'  #other place where I saved the data (can be changed)
    args.use_action = True
    args.batch_size = 32
    args.seq_len = 512
    args.history_length = 5
    args.lexical = False
    args.no_history = False
    args.overwrite_cache = False

    data = MultiWozDataset(args, split=split, shuffle=False)

    history_raw_new = []
    action_raw_new = []
    output_raw_new = []
    belief_raw_new = []

    if split == 'test':
        test_dict = {}

    # populate dictionary with data
    delex_dict = {}
    for d in data:
        delex_dict[d['name']] = d

    # iterate on conversations
    for key in delex_dict:
        d = delex_dict[key]
        inp = d['input_raw']
        out = d['target_raw']
        goal = multiwoz_data[key]['goal']

        for turn, (sys, usr) in enumerate(zip(inp, out)):
            # remember that usr comes before system
            # 0 - usr target
            # 1 - context usr, sys - usr is target
            if turn == 0:
                #idk what to do here yet
                history_new = '<|context|> <|endofcontext|>'
                pass
            else:
                tmp_new = ['<|context|>']
                for k in range(turn):

                    tmp_new.append('<|user|> ' + out[k].strip())
                    tmp_new.append('<|system|> ' + inp[k].strip())

                tmp_new.append('<|endofcontext|>')
                history_new = ' '.join(tmp_new)

            # update history and add response
            history_raw_new.append(history_new)
            output_raw_new.append('<|response|> ' + usr.strip() + ' <|endofresponse|>')

        # add action
        action = d['usr_act']
        for act in action:
            tmp_act_new = []
            for i, a in enumerate(act):
                if i == len(act) - 1:
                    tmp_act_new.append(' '.join(a))
                else:
                    tmp_act_new.append(' '.join(a))
            if len(tmp_act_new) == 0:
                tmp_act_new.append(' ')
            tmp_new = '<|action|> {} <|endofaction|>'.format(' , '.join(tmp_act_new))
            action_raw_new.append(tmp_new)

        # TODO: add goal
        for turn in range(len(inp)):
            dialogue = multiwoz_data[key]
            goal = dialogue['goal']
            belief_state = get_belief_state(dialogue['log'][2*turn+1]['metadata'])  #adjust position
            
            tmp_bs_new = []
            for i, b in enumerate(belief_state):
                if b[-1] in ['not mentioned']: # comment this for DST task
                    continue
                if i == len(belief_state) - 1:
                    tmp_bs_new.append(' '.join(b))
                else:
                    tmp_bs_new.append(' '.join(b))
            
            if len(tmp_bs_new) == 0:
                tmp_bs_new.append(' ')
            
            tmp_new = '<|belief|> {} <|endofbelief|>'.format(' , '.join(tmp_bs_new))
            belief_raw_new.append(tmp_new)
        
               
        

    # dataset name - history_belief_action_sys_delex
    # history, goal?, action, target
    out_file_name = '{}/{}2.history_action_usr_delex'.format(save_dir, split)  #not to overwrite, add 2

    print('*** Writing to output file: {}'.format(out_file_name))
    tmp = []
    for hist, belf, act, trg in zip(history_raw_new, belief_raw_new, action_raw_new, output_raw_new):
        tmp.append(' '.join([hist.lower(), belf.lower(), act.lower(), trg.lower()]))
    with open(out_file_name, 'wt', encoding='utf8') as f:
        for ix, it in enumerate(tmp):
            if ix < 10:
                print('{} {} {}\n'.format(gpt2_tokenizer._bos_token, it.lower(), gpt2_tokenizer._eos_token))
            f.write('{} {} {}\n'.format(gpt2_tokenizer._bos_token, it.lower(), gpt2_tokenizer._eos_token))









