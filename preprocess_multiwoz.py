# -*- coding: utf-8 -*-
import copy
import json
import os
import sys
import re
import shutil
import urllib
from urllib import request
from collections import OrderedDict
from io import BytesIO
from zipfile import ZipFile
from tqdm import tqdm

import numpy as np

from utils.multiwoz import dbPointer
from utils.multiwoz import delexicalize

from utils.multiwoz.nlp import normalize, normalize_lexical, normalize_beliefstate, normalize_mine
import ipdb

np.set_printoptions(precision=3)

np.random.seed(2)

# GLOBAL VARIABLES
DICT_SIZE = 1000000
MAX_LENGTH = 600

DATA_DIR = './resources'


def is_ascii(s):
    return all(ord(c) < 128 for c in s)


# TODO: Make sure this works
def createDict(word_freqs):
    words = [k for k in word_freqs.keys()]
    freqs = [v for v in word_freqs.values()]

    sorted_idx = np.argsort(freqs)
    sorted_words = [words[ii] for ii in sorted_idx[::-1]]

    # Extra vocabulary symbols
    _GO = '_GO'
    EOS = '_EOS'
    UNK = '_UNK'
    PAD = '_PAD'
    SEP0 = '_SEP0'
    SEP1 = '_SEP1'
    SEP2 = '_SEP2'
    SEP3 = '_SEP3'
    SEP4 = '_SEP4'
    SEP5 = '_SEP5'
    SEP6 = '_SEP6'
    SEP7 = '_SEP7'
    extra_tokens = [_GO, EOS, UNK, PAD, SEP0, SEP1, SEP2, SEP3, SEP4, SEP5, SEP6, SEP7]
    # extra_tokens = [_GO, EOS, UNK, PAD]

    worddict = OrderedDict()
    for ii, ww in enumerate(extra_tokens):
        worddict[ww] = ii
    for ii, ww in enumerate(sorted_words):
        worddict[ww] = ii #+ len(extra_tokens)

    new_worddict = worddict.copy()
    for key, idx in worddict.items():
        if idx >= DICT_SIZE:
            del new_worddict[key]
    return new_worddict


def buildDictionaries(word_freqs_usr, word_freqs_sys, word_freqs_histoy, lexicalize=False):
    """Build dictionaries for both user and system sides.
    You can specify the size of the dictionary through DICT_SIZE variable."""
    dicts = []
    worddict_usr = createDict(word_freqs_usr)
    dicts.append(worddict_usr)
    worddict_sys = createDict(word_freqs_sys)
    dicts.append(worddict_sys)
    worddict_history = createDict(word_freqs_histoy)
    dicts.append(worddict_history)

    # reverse dictionaries
    idx2words = []
    for dictionary in dicts:
        dic = {}
        for k, v in dictionary.items():
            dic[v] = k
        idx2words.append(dic)

    # NOTE: I swapped input and output here because usr is output and input is system
    if lexicalize:
        input_index2word_filename = os.path.join(DATA_DIR, 'output_lang.index2word_lexicalized.json')
        input_word2index_filename = os.path.join(DATA_DIR, 'output_lang.word2index_lexicalized.json')
        output_index2word_filename = os.path.join(DATA_DIR, 'input_lang.index2word_lexicalized.json')
        output_word2index_filename = os.path.join(DATA_DIR, 'input_lang.word2index_lexicalized.json')
        history_index2word_filename = os.path.join(DATA_DIR, 'history_lang.index2word_lexicalized.json')
        history_word2index_filename = os.path.join(DATA_DIR, 'history_lang.word2index_lexicalized.json')
    else:
        input_index2word_filename = os.path.join(DATA_DIR, 'output_lang.index2word.json')
        input_word2index_filename = os.path.join(DATA_DIR, 'output_lang.word2index.json')
        output_index2word_filename = os.path.join(DATA_DIR, 'input_lang.index2word.json')
        output_word2index_filename = os.path.join(DATA_DIR, 'input_lang.word2index.json')
        history_index2word_filename = os.path.join(DATA_DIR, 'history_lang.index2word.json')
        history_word2index_filename = os.path.join(DATA_DIR, 'history_lang.word2index.json')

    with open(input_index2word_filename, 'w') as f:
        json.dump(idx2words[0], f, indent=2)
    with open(input_word2index_filename, 'w') as f:
        json.dump(dicts[0], f, indent=2)
    with open(output_index2word_filename, 'w') as f:
        json.dump(idx2words[1], f, indent=2)
    with open(output_word2index_filename, 'w') as f:
        json.dump(dicts[1], f, indent=2)
    with open(history_index2word_filename, 'w') as f:
        json.dump(idx2words[2], f, indent=2)
    with open(history_word2index_filename, 'w') as f:
        json.dump(dicts[2], f, indent=2)


def fixDelex(filename, data, data2, idx, idx_acts):
    """Given system dialogue acts fix automatic delexicalization."""
    try:
        turn = data2[filename.strip('.json')][str(idx_acts)]
    except:
        return data

    # if not isinstance(turn, str) and not isinstance(turn, unicode):
    if not isinstance(turn, bytes) and not isinstance(turn, str):
        for k, act in turn.items():
            if 'Attraction' in k:
                if 'restaurant_' in data['log'][idx]['text']:
                    data['log'][idx]['text'] = data['log'][idx]['text'].replace("restaurant", "attraction")
                if 'hotel_' in data['log'][idx]['text']:
                    data['log'][idx]['text'] = data['log'][idx]['text'].replace("hotel", "attraction")
            if 'Hotel' in k:
                if 'attraction_' in data['log'][idx]['text']:
                    data['log'][idx]['text'] = data['log'][idx]['text'].replace("attraction", "hotel")
                if 'restaurant_' in data['log'][idx]['text']:
                    data['log'][idx]['text'] = data['log'][idx]['text'].replace("restaurant", "hotel")
            if 'Restaurant' in k:
                if 'attraction_' in data['log'][idx]['text']:
                    data['log'][idx]['text'] = data['log'][idx]['text'].replace("attraction", "restaurant")
                if 'hotel_' in data['log'][idx]['text']:
                    data['log'][idx]['text'] = data['log'][idx]['text'].replace("hotel", "restaurant")

    return data


def delexicaliseReferenceNumber(sent, turn):
    """Based on the belief state, we can find reference number that
    during data gathering was created randomly."""
    domains = ['restaurant', 'hotel', 'attraction', 'train', 'taxi', 'hospital']  # , 'police']
    if turn['metadata']:
        for domain in domains:
            if turn['metadata'][domain]['book']['booked']:
                for slot in turn['metadata'][domain]['book']['booked'][0]:
                    if slot == 'reference':
                        val = '[' + domain + '_' + slot + ']'
                    else:
                        val = '[' + domain + '_' + slot + ']'
                    key = normalize(turn['metadata'][domain]['book']['booked'][0][slot])
                    sent = (' ' + sent + ' ').replace(' ' + key + ' ', ' ' + val + ' ')

                    # try reference with hashtag
                    key = normalize("#" + turn['metadata'][domain]['book']['booked'][0][slot])
                    sent = (' ' + sent + ' ').replace(' ' + key + ' ', ' ' + val + ' ')

                    # try reference with ref#
                    key = normalize("ref#" + turn['metadata'][domain]['book']['booked'][0][slot])
                    sent = (' ' + sent + ' ').replace(' ' + key + ' ', ' ' + val + ' ')
    return sent


def createDelexData():
    """Main function of the script - loads delexical dictionary,
    goes through each dialogue and does:
    1) data normalization
    2) delexicalization
    3) addition of database pointer
    4) saves the delexicalized data
    """
    # create dictionary of delexicalied values that then we will search against, order matters here!
    dic = delexicalize.prepareSlotValuesIndependent()
    delex_data = {}

    fin1 = open(os.path.join(DATA_DIR, 'multi-woz/data.json'))
    data = json.load(fin1)

    fin2 = open(os.path.join(DATA_DIR, 'multi-woz/dialogue_acts.json'))
    data2 = json.load(fin2)

    for dialogue_name in tqdm(data):
        dialogue = data[dialogue_name]
        # print dialogue_name

        idx_acts = 1
        for turn in dialogue['log']:
            idx = turn['turn_id']
            # normalization, split and delexicalization of the sentence
            sent = normalize(turn['text'])

            words = sent.split()
            sent = delexicalize.delexicalise(' '.join(words), dic)

            # parsing reference number GIVEN belief state
            sent = delexicaliseReferenceNumber(sent, turn)

            # changes to numbers only here
            digitpat = re.compile('\d+')
            sent = re.sub(digitpat, '[value_count]', sent)

            # delexicalized sentence added to the dialogue
            dialogue['log'][idx]['text'] = sent

            '''
            FIXME: I think we might not need this cause we don't care about the system
            if idx % 2 == 1:  # if it's a system turn
                # add database pointer
                pointer_vector = addDBPointer(turn)
                # add booking pointer
                pointer_vector = addBookingPointer(dialogue, turn, pointer_vector)

                # print pointer_vector
                dialogue['log'][idx - 1]['db_pointer'] = pointer_vector.tolist()
            '''

            # FIXME: Literally not sure if we need this line
            dialogue = fixDelex(dialogue_name, dialogue, data2, idx, idx_acts)
            idx_acts += 1

        delex_data[dialogue_name] = dialogue

    with open(os.path.join(DATA_DIR, 'multi-woz/delex.json'), 'w') as outfile:
        json.dump(delex_data, outfile)

    return delex_data


def analyze_dialogue_raw_beliefstate(dialogue, maxlen):
    """Cleaning procedure for all kinds of errors in text and annotation."""
    d = dialogue
    # do all the necessary postprocessing
    if len(d['log']) % 2 != 0:
        # print path
        print('odd # of turns')
        return None  # odd number of turns, wrong dialogue
    d_pp = {}
    d_pp['goal'] = d['goal']  # for now we just copy the goal
    usr_turns = []
    sys_turns = []
    for i in range(len(d['log'])):
        if len(d['log'][i]['text'].split()) > maxlen:
            print('too long')
            return None  # too long sentence, wrong dialogue
        if i % 2 == 0:  # usr turn
            text = d['log'][i]['text']
            if not is_ascii(text):
                print('not ascii')
                return None
            usr_turns.append(d['log'][i])
        else:  # sys turn
            if len(d['log'][i]['metadata']) == 0:
                print('2 user turns')
                return None  # probably 2 usr turns
            text = d['log'][i]['text']
            if not is_ascii(text):
                print('not ascii')
                return None
            sys_turns.append(d['log'][i])
    d_pp['usr_log'] = usr_turns
    d_pp['sys_log'] = sys_turns

    return d_pp


def get_dial_raw(dialogue):
    """Extract a dialogue from the file"""
    dial = []
    d_orig = analyze_dialogue_raw_beliefstate(dialogue, MAX_LENGTH)  # max turn len is 50 words
    if d_orig is None:
        return None
    usr = [t['text'] for t in d_orig['usr_log']]
    sys = [t['text'] for t in d_orig['sys_log']]
    actions = [t['dialog_act'] for t in d_orig['usr_log']]
    goal = d_orig['goal']
    for u, s, a in zip(usr, sys, actions):
        dial.append((u, s, a))

    return dial, goal


def get_action(action, dial_name):
    # some catch i don't understand
    if isinstance(action, str):
        return action, []

    acts = {}
    for k, v in action.items():
        domain, act = [w.lower() for w in k.split('-')]
        for (slot, value) in v:
            slot = ' '.join(slot.lower().strip().split('\t'))
            value = ' '.join(value.lower().strip().split('\t'))

            if domain in acts and act in acts[domain] and slot in acts[domain][act]:
                continue
            if domain not in acts:
                acts[domain] = {}
                acts[domain][act] = {}
                acts[domain][act] = [(slot, value)]
            elif act not in acts[domain]:
                acts[domain][act] = {}
                acts[domain][act] = [(slot, value)]
            else:
                acts[domain][act].append((slot, value))

    concat = []
    for domain in acts:
        for act in acts[domain]:
            for slot, value in acts[domain][act]:
                concat.append((domain, act, slot))
    return action, concat


def divideData(data, lexicalize=False):
    """Given test and validation sets, divide
    the data for three different sets"""
    # get train/test/val list
    testListFile = []
    with open(os.path.join(DATA_DIR, 'multi-woz/testListFile.json')) as fin:
        for line in fin:
            testListFile.append(line[:-1])

    valListFile = []
    with open(os.path.join(DATA_DIR, 'multi-woz/valListFile.json')) as fin:
        for line in fin:
            valListFile.append(line[:-1])

    trainListFile = open(os.path.join(DATA_DIR, 'multi-woz/trainListFile'), 'w')

    ## For holding the dialogues
    test_dials = {}
    val_dials = {}
    train_dials = {}

    # dictionaries for words
    word_freqs_usr = OrderedDict()
    word_freqs_sys = OrderedDict()
    word_freqs_history = OrderedDict()
    #word_freqs_action = OrderedDict()

    for dialogue_name in tqdm(data):

        ## store the features we will use later
        dial, goal = get_dial_raw(data[dialogue_name])

        # Catch errors
        if not dial:
            continue

        dialogue = {}
        dialogue['usr'] = []
        dialogue['sys'] = []
        dialogue['usr_act_raw'] = []
        dialogue['usr_act'] = []

        for turn_id, turn in enumerate(dial):
            dialogue['usr'].append(turn[0])
            dialogue['sys'].append(turn[1])

            ## FIXME: This is where we use the usr action
            turn_act_raw, turn_act = get_action(turn[2], dialogue_name)
            dialogue['usr_act_raw'].append(turn_act_raw)
            dialogue['usr_act'].append(turn_act)


        if dialogue_name in testListFile:
            test_dials[dialogue_name] = dialogue
        elif dialogue_name in valListFile:
            val_dials[dialogue_name] = dialogue
        else:
            trainListFile.write(dialogue_name + '\n')
            train_dials[dialogue_name] = dialogue

        ## preparation of other things
        for turn in dial:
            # user turn
            line = turn[0]
            words_in = line.strip().split(' ')
            for w in words_in:
                if w not in word_freqs_usr:
                    word_freqs_usr[w] = 0
                word_freqs_usr[w] += 1

            # dialogue history vocab
            for w in words_in:
                if w not in word_freqs_history:
                    word_freqs_history[w] = 0
                word_freqs_history[w] += 1

            # sys turn
            line = turn[1]
            words_in = line.strip().split(' ')
            for w in words_in:
                if w not in word_freqs_sys:
                    word_freqs_sys[w] = 0
                word_freqs_sys[w] += 1

            # dialogue history vocab
            for w in words_in:
                if w not in word_freqs_history:
                    word_freqs_history[w] = 0
                word_freqs_history[w] += 1

        act_words = []
        for dial_act in dialogue['usr_act']:
            for domain, act, slot in dial_act:
                act_words.extend([domain, act, slot])
        for w in act_words:
            if w not in word_freqs_usr:
                word_freqs_usr[w] = 0
            word_freqs_usr[w] += 1
            if w not in word_freqs_history:
                word_freqs_history[w] = 0
            word_freqs_history[w] += 1
            #if w not in word_freqs_action:
            #    word_freqs_action[w] = 0
            #word_freqs_action[w] += 1



    # save all dialogues
    if lexicalize:
        val_filename = os.path.join(DATA_DIR, 'val_dials_lexicalized.json')
        test_filename = os.path.join(DATA_DIR, 'test_dials_lexicalized.json')
        train_filename = os.path.join(DATA_DIR, 'train_dials_lexicalized.json')
    else:
        val_filename = os.path.join(DATA_DIR, 'val_dials.json')
        test_filename = os.path.join(DATA_DIR, 'test_dials.json')
        train_filename = os.path.join(DATA_DIR, 'train_dials.json')

    with open(val_filename, 'w') as f:
        json.dump(val_dials, f, indent=4)

    with open(test_filename, 'w') as f:
        json.dump(test_dials, f, indent=4)

    with open(train_filename, 'w') as f:
        json.dump(train_dials, f, indent=4)

    return word_freqs_usr, word_freqs_sys, word_freqs_history


def main():
    overwrite = False
    if len(sys.argv) > 1:
        overwrite = True

    if not os.path.isfile(os.path.join(DATA_DIR, 'multi-woz/delex.json')) or overwrite:
        data = createDelexData()
    else:
        data = json.load(open(os.path.join(DATA_DIR, 'multi-woz/delex.json')))

    '''
    elif sys.argv[1] == 'lexical':
        print('MultiWoz Create lexicalized dialogues. Get yourself a coffee, this might take a while.')

        if not os.path.isfile(os.path.join(DATA_DIR, 'multi-woz/lex.json')):
            data = createLexicalData()
        else:
            data = json.load(open(os.path.join(DATA_DIR, 'multi-woz/lex.json')))

    else:
        raise TypeError('unknown preprocessing')
    '''
    print('delex.json built, check for correctness')
    print('Divide dialogues for separate bits - usr, sys, db, bs')
    word_freqs_usr, word_freqs_sys, word_freqs_history = divideData(data,
                                                                    lexicalize=False)#(str(sys.argv[1])=='lexical'))

    print('Building dictionaries')
    buildDictionaries(word_freqs_usr, word_freqs_sys, word_freqs_history,
                      lexicalize=False)#(str(sys.argv[1])=='lexical'))



if __name__ == "__main__":
    main()